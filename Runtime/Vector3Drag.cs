using UnityEngine;

namespace JonathanDefraiteur.PhysicsHelper.Runtime
{
    [RequireComponent(typeof(Rigidbody))]
    public class Vector3Drag : MonoBehaviour
    {
        private Rigidbody _rigidbody;
        [SerializeField] private Vector3 drag;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            if (null == _rigidbody) {
                Debug.LogError("No Rigidbody component found.");
                enabled = false;
            }

            _rigidbody.drag = 0;
        }

        private void FixedUpdate()
        {
            PhysicsMethods.ApplyDrag(_rigidbody, drag);
        }
    }
}