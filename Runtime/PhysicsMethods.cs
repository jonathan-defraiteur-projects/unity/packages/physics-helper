﻿using UnityEngine;

namespace JonathanDefraiteur.PhysicsHelper.Runtime
{
    public class PhysicsMethods
    {
        public static void ApplyDrag(Rigidbody _rigidbody, float _drag)
        {
            ApplyDrag(_rigidbody, _drag, Time.fixedDeltaTime);
        }
        
        public static void ApplyDrag(Rigidbody _rigidbody, float _drag, float _deltaTime)
        {
            _rigidbody.velocity *= 1 - _deltaTime * _drag;
        }
        
        public static void ApplyDrag(Rigidbody _rigidbody, Vector3 _drag)
        {
            ApplyDrag(_rigidbody, _drag, Time.fixedDeltaTime);
        }
        
        public static void ApplyDrag(Rigidbody _rigidbody, Vector3 _drag, float _deltaTime)
        {
            Vector3 velocity = _rigidbody.transform.InverseTransformVector(_rigidbody.velocity);
            
            Vector3 newVelocity = new Vector3(
                velocity.x * (1 - _deltaTime * _drag.x),
                velocity.y * (1 - _deltaTime * _drag.y),
                velocity.z * (1 - _deltaTime * _drag.z)
                );

            _rigidbody.velocity = _rigidbody.transform.TransformVector(newVelocity);
        }
    }
}
